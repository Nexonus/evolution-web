package com.it.datasquirl.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Utility class for Hibernate. Manages the sessions.
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            return new Configuration().configure().buildSessionFactory();
        } catch (Exception e) {
            System.out.println("----> SessionFactory creation failed:     " + e.getMessage());
            throw new ExceptionInInitializerError(e);
        }
    }

    /**
     * Returns a Hibernate session
     * @return returns a Hibernate session
     */
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    private static void closeSession() {
        getSessionFactory().close();
    }

}
