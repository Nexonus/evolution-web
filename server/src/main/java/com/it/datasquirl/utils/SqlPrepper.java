package com.it.datasquirl.utils;

import com.it.datasquirl.controller.UserController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Prepares a template SQL for further use.
 * Reads a file from the resources folder and injects the necessary variables into it.
 */
public class SqlPrepper {

    private static Logger log = LoggerFactory.getLogger(UserController.class);

    private static final String SQL_FOLDER = "/sql/";
    private static final String SET_ACTIVE_DB_FILE = "setActiveDb.sql";
    private static final String CREATE_DB_FILE = "createDb.sql";
    private static final String DELETE_DB_FILE = "deleteDb.sql";
    private static final String GET_ALL_TABLES_FROM_DB_FILE = "getAllTablesFromDb.sql";
    private static final String CREATE_TABLE_FILE = "createTable.sql";
    private static final String DELETE_TABLE_FILE = "deleteTable.sql";
    private static final String GET_ALL_DATA_FROM_TABLE_FILE = "getAllDataFromTable.sql";
    private static final String ADD_COLUMN_FILE = "addColumn.sql";
    private static final String ASSIGN_DB_TO_USER_FILE = "assignDbToUser.sql";
    private static final String PUT_DATA_IN_CELL = "putDataInCell.sql";

    public String setActiveDbStatement(String activeDb) {
        String sql = readSqlFromResources(SET_ACTIVE_DB_FILE);

        return String.format(sql, activeDb);
    }

    public String createDbStatement(String dbName) {
        String sql = readSqlFromResources(CREATE_DB_FILE);

        return String.format(sql, dbName);
    }

    public String deleteDbStatement(String dbName) {
        String sql = readSqlFromResources(DELETE_DB_FILE);

        return String.format(sql, dbName);
    }

    public String getAllTablesFromDbStatement() {
        return readSqlFromResources(GET_ALL_TABLES_FROM_DB_FILE);
    }

    public String createTableStatement(String tableName) {
        String sql = readSqlFromResources(CREATE_TABLE_FILE);

        return String.format(sql, tableName);
    }

    public String deleteTableStatement(String tableName) {
        String sql = readSqlFromResources(DELETE_TABLE_FILE);

        return String.format(sql, tableName);
    }

    public String getAllDataFromTableStatement(String tableName) {
        String sql = readSqlFromResources(GET_ALL_DATA_FROM_TABLE_FILE);

        return String.format(sql, tableName);
    }

    public String addColumn(String tableName, String columnName, String columnType) {
        String sql = readSqlFromResources(ADD_COLUMN_FILE);

        return String.format(sql, tableName, columnName, columnType);
    }

    public String assignDbToUserStatement(String dbName, int userId) {
        String sql = readSqlFromResources(ASSIGN_DB_TO_USER_FILE);

        return String.format(sql, dbName, userId);
    }

    public String putDataInCell(String tableName, String columnHeader, int rowId, String content) {
        String sql = readSqlFromResources(PUT_DATA_IN_CELL);

        return String.format(sql, tableName, columnHeader, content, rowId);
    }

    /**
     * Reads the sql file and converts it into a String.
     * @param sqlFileName the file name of the sql that's needed, e.g. "example.sql"
     * @return the sql as a String
     */
    private String readSqlFromResources(String sqlFileName) {
        String result = "";

        try {
            result = new String(Files.readAllBytes(
                    Paths.get(getClass().getResource(SQL_FOLDER + sqlFileName).toURI())));
        } catch (IOException e) {
            log.error("IOException when reading file {}", sqlFileName);
        } catch (URISyntaxException e) {
            log.error("URISyntaxException when reading file {}", sqlFileName);
        } catch (NullPointerException e) {
            log.error("NullPointerException when reading file {}", sqlFileName);
        }

        return result;
    }
}
