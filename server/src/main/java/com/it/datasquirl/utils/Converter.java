package com.it.datasquirl.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Converter class. Put all conversions here.
 */
public class Converter {

    /**
     * Converts a strange Map-In-List construct and returns a simple List.
     * @param inputList needs to be a map in a list, List<Map>
     * @return a simple list, without the map
     */
    public static List convertMapInListToList(List<Map> inputList) {
        List outputList = new ArrayList();
        
        inputList.forEach((item) -> {
            String key = new ArrayList<String>(item.keySet()).get(0);
            String value = item.get(key).toString();
            
            outputList.add(value);
        });
        
        return outputList;
    }
}
