package com.it.datasquirl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot Application Starter
 */
@SpringBootApplication
public class ApplicationStarter {

    /**
     * Starts the Spring Boot Application
     * @param args arguments given to the application by the commandline
     */
    public static void main(String[] args) {
        SpringApplication.run(ApplicationStarter.class, args);
    }
    
}
