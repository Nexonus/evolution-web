/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.datasquirl.objects;

import java.time.LocalDateTime;
import java.util.Date;

/**
 *
 * @author HHOEPER
 */
public class Content {

    Object content;
    ContentTypes contentType;
    
    public Content(Object content){
    this.content = content;
    this.contentType = getType();
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    private ContentTypes getType() {
        if (content instanceof String) {
            return ContentTypes.STRING;
        } 
        if (content instanceof Boolean) {
            return ContentTypes.BOOLEAN;
        } 
        if (content instanceof Integer) {
            return ContentTypes.INTEGER;
        }
        if (content instanceof Float) {
            return ContentTypes.FLOAT;
        }
        if (content instanceof Date || content instanceof LocalDateTime) {
            return ContentTypes.DATE;
        }
        return ContentTypes.UNKNOWN;
    }

    public ContentTypes getContentType() {
        return contentType;
    }
    
    
}
