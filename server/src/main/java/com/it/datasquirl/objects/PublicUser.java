package com.it.datasquirl.objects;

/**
 * Public user, that contains no password. Used for sending it to the frontend.
 */
public class PublicUser {

    private int id;
    private String userName;

    public PublicUser(int id, String userName) {
        this.id = id;
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

}
