package com.it.datasquirl.objects;

import javax.persistence.*;
import javax.persistence.Column;
import javax.persistence.Table;

@Entity
@Table(name = "dbs")
public class Databases {
    
    @Id
    @Column(name = "id_db")
    private int id;
    
    @Column(name = "dbname")
    private String dbName;
    
    @Column(name = "id_user_fk")
    private int idUserFk;
    
    public int getId() {
        return id;
    }
    
    public String getDbName() {
        return dbName;
    }
    
    public int getIdUserFk() {
        return idUserFk;
    }
}
