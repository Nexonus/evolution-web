/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.datasquirl.objects;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HHOEPER
 */
public class Column {

    String header;
    List<Content> content;
    Boolean isPk;

    public Column(String header, List<Object> content) {
        this.header = header;

        this.content = new ArrayList<>();

        for (Object contentObject : content) {
            this.content.add(new Content(contentObject));
        }

        this.isPk = false;
    }
    
    public Column (String header){
        this.header = header;
        this.content = new ArrayList<>();
    }

    public Column(String header, List<Object> content, Boolean isPk) {
        this.header = header;

        this.content = new ArrayList<>();
        for (Object contentObject : content) {
            this.content.add(new Content(contentObject));
        }

        this.isPk = isPk;
    }

    public Content getData(int row) {
        return this.content.get(row);
    }

    public Boolean checkIfInColumn(String searchString) {
        for (Content content : content) {
            switch (content.contentType) {
                case BOOLEAN:
                    if (content.content.equals(Boolean.parseBoolean(searchString)));
                    return true;
                case DATE:
                    SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                    String date = format.format(content.content);
                    if (date.equals(searchString));
                   return true;
                case FLOAT:
                    if (content.content.equals(Float.parseFloat(searchString)));
                    return true;
                case INTEGER:
                    if (content.content.equals(Integer.parseInt(searchString)));
                    return true;
                case STRING:
                    if (content.content.equals(searchString));
                    return true;
                case UNKNOWN:
                    if (content.content.equals(searchString));
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

    public Boolean getIsPk() {
        return isPk;
    }

    public void setIsPk(Boolean isPk) {
        this.isPk = isPk;
    }

}
