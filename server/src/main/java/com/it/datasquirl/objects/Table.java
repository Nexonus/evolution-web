/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.datasquirl.objects;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author HHOEPER
 */
public class Table {
    private Logger log = LoggerFactory.getLogger(Table.class);
    String name;
    int uId;
    String creationDate;
    String lastModified;
    List<Column> columns;
    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
    
    public Table(){
        this.columns = new LinkedList<>();
    }
    
    public Table(String name, Date creationDate, Date lastModified, List<Column> columns,int uId) {
        this.name = name;
        this.creationDate = formatter.format(creationDate);
        this.lastModified = formatter.format(lastModified);
        this.columns = columns;
        this.uId = uId;
    }
    
    public Boolean addColumn(String Header, List<Object> content){
        this.columns.add(new Column(Header, content));
        return true;
    }
    
        public Boolean addColumn(Column col){
        this.columns.add(col);
        return true;
    }
    
    public boolean  createNewEmptyColumn(String Header){
        this.columns.add(new Column(Header));
        return true;
    }
    
    public Boolean addContent(int colNumber, Content content){
        this.columns.get(colNumber).content.add(content);
        return true;
    }
    
    
    public Column getColumn(String header){
        for (Column column : columns) {
            if (column.header.equalsIgnoreCase(header)){
                log.info("Column " + header + "was found");
                return column;
            }
        }
        log.info("Column " + header + "not found");
        return null;
        
    }
    
    public Column getColumn(Integer number){
        return columns.get(number);
    }
    
    public Content getContent(Column col, int rowNumber){
        return col.getData(rowNumber);
    }
    
    public Boolean checkIfInTable(String searchStr){
        for(Column col:columns){
            if(col.checkIfInColumn(searchStr)){
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }
     
}
