package com.it.datasquirl.objects;


import com.it.datasquirl.utils.PasswordService;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Private user that contains all information. Only used inside of the backend, due to security.
 */
@Entity
@Table(name = "user")
public class PrivateUser {

    @Id
    @Column(name = "id_user")
    private int id;

    @Column(name = "username")
    private String userName;

    @Column(name = "password")
    private String password;

    public PrivateUser(String username, String password) throws Exception {
        this.userName = username;
        this.password = PasswordService.getSaltedHash(password);
    }

    public PrivateUser() {
        this.userName = "";
        this.password = "";
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public PublicUser getPublicUser() {
        return new PublicUser(this.id, this.userName);
    }
}
