/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.datasquirl.objects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author HHOEPER
 */
public class DataBase {
    int uId;
    String name;
    List<Table> tables = new ArrayList<>();

    public DataBase(){
        
    }
    public DataBase(String name, int uId,List<Table> tables){
        this.uId = uId;
        this.name = name;
        this.tables = tables;
    }
    
    
    public Boolean addTable(String tableName, List<String> headers, int id){
        List<Column> columns = new ArrayList<>();
        for (String header : headers) {
            columns.add(new Column(header));
        }
        Date date = new Date();
        this.tables.add(new Table(tableName, date, date,columns,id));  //get uid somehow
        return true;
    }
    
    public List<Table> getTables() {
        return tables;
    }

    public void setTables(List<Table> tables) {
        this.tables = tables;
    }

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
    
}
