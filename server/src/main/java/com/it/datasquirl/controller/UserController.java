package com.it.datasquirl.controller;

import com.it.datasquirl.objects.Databases;
import com.it.datasquirl.objects.PrivateUser;
import com.it.datasquirl.objects.PublicUser;
import com.it.datasquirl.service.DatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This class provides multiple REST API Endpoints for controlling everything user related.
 * That includes:
 * - Registering a new user
 * - Logging a user in
 * - Getting all databases that belong to one given user
 */
@RestController()
@CrossOrigin(value = "http://localhost:8000")
@RequestMapping(value = "/user")
public class UserController {
    
    private Logger log = LoggerFactory.getLogger(UserController.class);
    
    private DatabaseService dbService;
    
    @Autowired
    public UserController(DatabaseService dbService) {
        this.dbService = dbService;
    }

    /**
     * Logs the user in
     * @param userName the user name
     * @param password the user password
     * @return returns the public user for the successfully logged in user
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @CrossOrigin(value = "http://localhost:8000")
    public ResponseEntity<PublicUser> login(@RequestParam(value = "username") String userName,
                                            @RequestParam(value = "password") String password) {
        
        PrivateUser privateUser = dbService.getUser(userName);
        
        if (privateUser == null) {
            log.info("User not found");
            return new ResponseEntity<>((PublicUser) null, HttpStatus.NOT_FOUND);
        }
        
        if (dbService.checkPassword(password, privateUser.getPassword())) {
            return new ResponseEntity<>(privateUser.getPublicUser(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>((PublicUser) null, HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * Registers a new user
     * @param userName the user name
     * @param password the user password
     * @return returns the public user for the successfully registered in user
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<PublicUser> register(@RequestParam(value = "username") String userName,
                                               @RequestParam(value = "password") String password) {
        
        try {
            PrivateUser userFromDb = dbService.getUser(userName);
            
            if (userFromDb != null && userFromDb.getUserName().equals(userName)) {
                log.warn("Already found user {} in DB", userName);
                return new ResponseEntity<>((PublicUser) null, HttpStatus.CONFLICT);
            }
            
            dbService.storeUserInDb(new PrivateUser(userName, password));
            log.info("Successfully stored user {} in DB", userName);
            
            // 2nd call is needed, so that the field "ID" is set in the userFromDb object
            userFromDb = dbService.getUser(userName);
            
            return new ResponseEntity<>(userFromDb.getPublicUser(), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exception occurred during user creation\n" + e.getMessage());
            
            return new ResponseEntity<>((PublicUser) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns all databases that a user "owns"
     * @param userId the user ID
     * @return returns a list of all databases from a user
     */
    @RequestMapping(value = "/getDbs", method = RequestMethod.GET)
    public ResponseEntity<List<Databases>> getDatabases(@RequestParam(value = "userId") int userId) {
        
        List<Databases> result = dbService.getAllDbsForUser(userId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
}
