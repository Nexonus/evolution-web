package com.it.datasquirl.controller;

import com.it.datasquirl.objects.Table;
import com.it.datasquirl.service.DatabaseService;
import com.it.datasquirl.service.DbTableMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * This class provides multiple REST API Endpoints for controlling everything table related.
 * That includes:
 * - Creating, Deleting tables
 * - Adding columns
 * - Getting the content of a table
 * - Updating a cell in a table
 */
@RestController
@RequestMapping(value = "/{userId}/db/{dbName}/table/{tableName}")
@CrossOrigin(value = "http://localhost:8000")
public class DatabaseTableController {

    private Logger log = LoggerFactory.getLogger(DatabaseTableController.class);

    private DatabaseService dbService;

    @Autowired
    public DatabaseTableController(DatabaseService dbService) {
        this.dbService = dbService;
    }

    /**
     * Returns all content from a table
     * @param dbName the database name
     * @param tableName the table name
     * @param userId the user ID
     * @return all table content
     */
    @RequestMapping(value = "/getTable", method = RequestMethod.GET)
    public ResponseEntity<Table> getTable(@PathVariable String dbName,
                                          @PathVariable String tableName,
                                          @PathVariable Integer userId) {

        DbTableMapper dbTableMapper = new DbTableMapper();

        List<Map<String, Object>> content = dbService.getAllDataFromTable(dbName, tableName);
        Table table = dbTableMapper.dbToTable(content);

        return new ResponseEntity<>(table, HttpStatus.OK);
    }

    /**
     * Creates a new table
     * @param dbName the database name
     * @param tableName the table name
     * @param userId the user ID
     * @return returns OK for success
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public HttpStatus createTable(@PathVariable String dbName,
                                  @PathVariable String tableName,
                                  @PathVariable Integer userId) {

        dbService.createTable(dbName, tableName);

        return HttpStatus.OK;
    }

    /**
     * Adds a new column to a table
     * @param userId the user ID
     * @param dbName the database name
     * @param tableName the table name
     * @param columnName the column name, self explanatory, right?
     * @param columnType the column type
     * @param columnLink the foreign key
     * @return returns OK for success
     */
    @RequestMapping(value = "/addColumn", method = RequestMethod.POST)
    public HttpStatus addColumn(@PathVariable Integer userId,
                                @PathVariable String dbName,
                                @PathVariable String tableName,
                                @RequestParam(value = "columnName") String columnName,
                                @RequestParam(value = "columnType") String columnType,
                                @RequestParam(value = "columnLink") int columnLink) {

        dbService.addColumn(dbName, tableName, columnName, columnType);

        return HttpStatus.OK;
    }

    /**
     * Deletes a table
     * @param dbName the database name
     * @param tableName the table name
     * @param userId the user ID
     * @return returns OK for success
     */
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public HttpStatus deleteTable(@PathVariable String dbName,
                                  @PathVariable String tableName,
                                  @PathVariable Integer userId) {

        dbService.deleteTable(dbName, tableName);

        log.info("Successfully deleted table \"{}\" in db \"{}\"", tableName, dbName);

        return HttpStatus.OK;
    }

    /**
     * Puts new data in one cell
     * @param dbName the database name
     * @param tableName the table name
     * @param columnHeader the column header
     * @param rowId the row ID
     * @param content the content for the cell
     * @return returns OK for success
     */
    @RequestMapping(value = "/putDataInCell", method = RequestMethod.POST)
    public HttpStatus putDataInCell(@PathVariable String dbName,
                                    @PathVariable String tableName,
                                    @RequestParam String columnHeader,
                                    @RequestParam int rowId,
                                    @RequestParam String content) {

        dbService.putDataInCell(dbName, tableName, columnHeader, rowId, content);

        return HttpStatus.OK;
    }

}
