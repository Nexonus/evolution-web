package com.it.datasquirl.controller;

import com.it.datasquirl.service.DatabaseService;
import com.it.datasquirl.utils.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This class provides multiple REST API Endpoints for controlling everything database related.
 * That includes:
 * - Creating, Deleting databases
 * - Getting all tables that are in one given database
 */
@RestController
@CrossOrigin(value = "http://localhost:8000")
@RequestMapping(value = "/{userId}/db/{dbName}")
public class DatabaseController {
    
    private Logger log = LoggerFactory.getLogger(DatabaseController.class);
    
    private DatabaseService dbService;
    
    @Autowired
    public DatabaseController(DatabaseService dbService) {
        this.dbService = dbService;
    }

    /**
     * Creates a new database.
     * @param userId the user ID
     * @param dbName the database name
     * @param templateName the template name
     * @return HTTP Status, OK for success
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public HttpStatus create(@PathVariable int userId,
                             @PathVariable String dbName,
                             @RequestParam(value = "templateName") String templateName) {
        
        if (templateName.isEmpty()) {
            dbService.createDb(dbName, userId);
        } else {
            dbService.createDb(dbName, userId, templateName);
        }
        
        log.info("Successfully created DB  \"{}\" with userID \"{}\"", dbName, userId);
        
        return HttpStatus.OK;
    }

    /**
     * Gets all tables the database contains
     * @param dbName the database name
     * @param userId the user ID
     * @return Response Entity which includes a list of all the tables
     */
    @RequestMapping(value = "/getTables", method = RequestMethod.GET)
    public ResponseEntity<List> getTables(@PathVariable String dbName,
                                          @PathVariable int userId) {
        
        List tables = dbService.getAllTablesFromDb(dbName);
        List output = Converter.convertMapInListToList(tables);
        
        return new ResponseEntity<>(output, HttpStatus.OK);
    }

    /**
     * Deletes a database
     * @param dbName the database name
     * @return HTTP Status, OK for success
     */
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public HttpStatus delete(@PathVariable String dbName) {
        
        dbService.deleteDb(dbName);
        log.info("Successfully deleted DB named {}", dbName);
        
        return HttpStatus.OK;
    }
    
}
