package com.it.datasquirl.service;

import com.it.datasquirl.objects.Databases;
import com.it.datasquirl.objects.PrivateUser;
import com.it.datasquirl.utils.HibernateUtil;
import com.it.datasquirl.utils.PasswordService;
import com.it.datasquirl.utils.SqlPrepper;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Does all operations on the database. Uses the SqlPrepper to get the necessary SQL Statements.
 */
@Transactional
@Repository
@SuppressWarnings("unchecked")
public class DatabaseService {
    
    private Logger log = LoggerFactory.getLogger(DatabaseService.class);
    
    private JdbcTemplate jdbcTemplate;
    private Session session;
    private SqlPrepper sqlPrepper = new SqlPrepper();

    /**
     * Gets the JDBC Template injected from the Spring framework
     * @param jdbcTemplate the JDBC template
     */
    @Autowired
    public DatabaseService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    public void createDb(String dbName, Integer userId) {
        String sqlDbCreation = sqlPrepper.createDbStatement(dbName);
        String sqlAssignDbToUser = sqlPrepper.assignDbToUserStatement(dbName, userId);
        
        sqlExecute(sqlDbCreation);
        sqlExecute(sqlAssignDbToUser);
    }
    
    public void createDb(String dbName, Integer userId, String templateName) {
    
    }
    
    public void deleteDb(String dbName) {
        String sql = sqlPrepper.deleteDbStatement(dbName);
        sqlExecute(sql);
    }
    
    public List getAllTablesFromDb(String dbName) {
        setActiveDb(dbName);
        String sql = sqlPrepper.getAllTablesFromDbStatement();
        return sqlQuery(sql);
    }
    
    public void createTable(String dbName, String tableName) {
        setActiveDb(dbName);
        
        String sql = sqlPrepper.createTableStatement(tableName);
        sqlExecute(sql);
    }
    
    public void deleteTable(String dbName, String tableName) {
        setActiveDb(dbName);
        
        String sql = sqlPrepper.deleteTableStatement(tableName);
        sqlExecute(sql);
    }
    
    public List getAllDataFromTable(String dbName, String table) {
        setActiveDb(dbName);
        
        String sql = sqlPrepper.getAllDataFromTableStatement(table);
        
        return sqlQuery(sql);
    }
    
    public void addColumn(String dbName, String tableName, String columnName, String columnType) {
        setActiveDb(dbName);
        
        String sql = sqlPrepper.addColumn(tableName, columnName, columnType);
        
        sqlExecute(sql);
    }
    
    public boolean checkPassword(String password, String storedPassword) {
        try {
            return PasswordService.check(password, storedPassword);
        } catch (Exception e) {
            log.error("Exception during Password Check\n" + e.getMessage());
            return false;
        }
    }
    
    public PrivateUser getUser(String userName) {
        List<PrivateUser> resultFromDb;
        
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            
            resultFromDb = session.createQuery("from PrivateUser").list();
        } catch (Exception sqlException) {
            log.error("----> An exception occured! \n" + sqlException.getMessage());
            
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
                log.error("----> Transaction got rolled back!");
            }
            
            return null;
        } finally {
            if (session != null) {
                session.close();
                log.info("----> Session closed!");
            }
        }
        
        for (PrivateUser element : resultFromDb) {
            if (element.getUserName().equals(userName)) {
                return element;
            }
        }
        
        return null;
    }

    public void storeUserInDb(PrivateUser privateUser) {
        session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(privateUser);
        session.getTransaction().commit();
        session.close();
    }

    public List<Databases> getAllDbsForUser(int userId) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            return session.createQuery("from Databases where idUserFk = :userId")
                    .setParameter("userId", userId)
                    .list();
        } catch (Exception sqlException) {
            log.error("----> An exception occured! \n" + sqlException.getMessage());

            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
                log.error("----> Transaction got rolled back!");
            }

            return null;
        } finally {
            if (session != null) {
                session.close();
                log.info("----> Session closed!");
            }
        }
    }

    public void putDataInCell(String dbName, String tableName, String columnHeader, int rowId, String content) {
        setActiveDb(dbName);

        String sql = sqlPrepper.putDataInCell(tableName, columnHeader, rowId, content);

        sqlExecute(sql);
    }

    private void setActiveDb(String dbName) {
        String sql = sqlPrepper.setActiveDbStatement(dbName);
        sqlExecute(sql);
    }

    private void sqlExecute(String sql) {
        jdbcTemplate.execute(sql);
    }

    private List sqlQuery(String sql) {
        return jdbcTemplate.query(sql, new ColumnMapRowMapper());
    }
}
