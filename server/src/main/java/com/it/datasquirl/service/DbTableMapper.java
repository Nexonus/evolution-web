/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.datasquirl.service;

import com.it.datasquirl.objects.Column;
import com.it.datasquirl.objects.Table;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author hendr
 */
public class DbTableMapper {

    public Table dbToTable(List<Map<String, Object>> input) {

        List<String> headers = getHeaders(input.get(0));
        List<Column> cols = new LinkedList<>();
        for (String header : headers) {
            Column col = new Column(header, getContent(input, header));
            cols.add(col);
        }
        Table table = new Table();
        cols.forEach((item) -> table.addColumn(item));
        return table;
    }

    private List<String> getHeaders(Map<String, Object> content) {
        List<String> headers = new LinkedList<>();

        for (Map.Entry<String, Object> entry : content.entrySet()) {
            headers.add(entry.getKey());
        }

        return headers;

    }

    private List<Object> getContent(List<Map<String, Object>> content, String header) {
        List<Object> contentList = new LinkedList<Object>();
        for (Map<String, Object> contentMap : content) {
            contentList.add(contentMap.get(header));
        }
        return contentList;

    }

}
