/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.datasquirl.service;

import com.it.datasquirl.objects.DataBase;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author HHOEPER
 */
public class JsonMapper {
    
        public Object mapFromJsonToObject(File file,Class mapTo) throws FileNotFoundException{
        Gson gson = new Gson();
        FileReader freader = new FileReader(file);
        JsonReader reader = new JsonReader(freader);
        Object ob = gson.fromJson(reader, mapTo);
        return ob;
    }
        
        public void mapToJson(List<DataBase> mapTo){
        
        try {
            ClassLoader cl = getClass().getClassLoader();
            File file = new File(cl.getResource("DummyJson/test.json").getFile());
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(file, mapTo);
        } catch (IOException ex) {
            System.out.println("worked not" + ex.getMessage());
        }
    }
        
            
    
}
