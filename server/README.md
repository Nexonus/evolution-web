# Spring Boot Server - Hosted in GCloud

## Basic Information

Static Server IP (GCloud), needs IP authentication on the GCloud side = 35.234.120.57:8080

`cloudbuild.yaml` is used for automatic builds of the server.

`dockerfile` is used for building the docker image.

## Running the server

### CMD

- Add your database here:
    - src/main/resources/application.properties
    - src/main/resources/hibernate.cfg.xml
- Make sure, that there is a database named "internal". Look at the file "Needed internal database for backend.png" for reference.
- Build the project using `mvn clean package`
- Run the program using: `java -jar PATH/TO/APP.jar`

## Docker

### Notes on the Dockerfile

This file is used as a plan on what to do when building the image. Note that there can only be one jar file in /target, 
because the dockerfile specifies to use `*.jar`.
Doing it like this enables us to change the version number of the app
without the need to change the dockerfile everytime.

## Kubernetes

### Creating a Kubernetes Cluster with Docker images

Source: [Tutorial on GCloud](https://cloud.google.com/kubernetes-engine/docs/tutorials/hello-app)

Got to the GCloud terminal, then:

1. Create a new folder for the project, then clone the git project

   `git clone https://Nexonus@bitbucket.org/Nexonus/evolution-web.git .`

2. cd into the folder where the dockerfile is (/server)
3. Build the project using `mvn clean package`
4. Export this variable for use in later commands

    `export PROJECT_ID="$(gcloud config get-value project -q)"`
    
    `export IMAGE_NAME="YOUR_NAME_HERE"`
    
    `export APP_NAME="YOUR_NAME_HERE"`
    
    `export CLUSTER_NAME="YOUR_NAME_HERE"`
   
5. Build the docker image

    `docker build -t gcr.io/${PROJECT_ID}/${IMAGE_NAME} .`
   
6. Check with `docker images` if the image was created

7. Upload the docker image to the private registry

    ```
    gcloud auth configure-docker                <--- Run this only once
    docker push gcr.io/${PROJECT_ID}/${IMAGE_NAME}
    ```
    Check, that the registry contains the image:
    ```
    gcloud container images list-tags gcr.io/${PROJECT_ID}/${IMAGE_NAME}
    ```
   
8. Create a Kubernetes Cluster

    `gcloud container clusters create ${CLUSTER_NAME} --num-nodes=3 --zone=europe-west3-a`
    
9. Deploy your application

    `kubectl run ${APP_NAME} --image=gcr.io/${PROJECT_ID}/${IMAGE_NAME} --port 8080`
    
10. Expose the app to the internet

    `kubectl expose deployment ${APP_NAME} --type=LoadBalancer --port 80 --target-port 8080`

### Scale the Cluster

1. Run this, for 3 replicas:

    `kubectl scale deployment ${APP_NAME} --replicas=3`
    
2. See the new instances running with:

    `kubectl get deployment ${APP_NAME}`

You can also scale it via the web GUI.

### Delete a cluster

1. Just do it via the web GUI

### Updating/Deploying a new docker image automatically

You do not need to do anything here, as there is a trigger in (GCloud) Cloud Build, which builds a new docker image every time there is a code change on the "master" branch in git. It also updates the image in the cluster.

### Updating/Deploying a new docker image manually

1. Build a new docker image (increment the variable `$IMAGE_NAME` first)

    `docker build -t gcr.io/${PROJECT_ID}/${IMAGE_NAME} .`
    
2. Push the new image to the registry

    `gcloud docker -- push gcr.io/${PROJECT_ID}/${IMAGE_NAME}`
    
3. Update the cluster

    `kubectl set image deployment/${APP_NAME} ${APP_NAME}=gcr.io/${PROJECT_ID}/${IMAGE_NAME}`

### Useful commands
- List running cluster: `gcloud container clusters list`