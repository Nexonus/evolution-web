import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import GridContainer from './GridContainer'
import Button from './Button'


const StyledLogin = styled(GridContainer)`
  padding: 0;
  
  .button {
    width: 100%;
  }
`
const StyledInput = styled.input`
  margin-bottom: 0.5rem;
  border: 0;
  padding: 0.2rem;
  width: 100%;
`

class Login extends React.Component {
  constructor(props) {
    super(props);
  }

  setName(event){
    this.setState({
      username:event.target.value
    })
  }

  setPassword(event){
    this.setState({
      password:event.target.value
    })
  }

  handleSubmit(event) {
    event.preventDefault()

    fetch('http://35.234.120.57:8080/api/user/login', {
      method: 'post',
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      })
    })
    .then(results => {
      return results.json()
    })
    .then(user => {
      this.props.authorize(user);
    })
  }

  render() {
    return (
      <StyledLogin>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <StyledInput type="text" name="name" placeholder="Name" onBlur={this.setName.bind(this)} />
          <StyledInput type="password" name="password" placeholder="Password" onBlur={this.setPassword.bind(this)}/>
          <Button type="submit" clickHandler={() => {}} className="button">Login</Button>
        </form>
      </StyledLogin>
    );
  }
}

Login.propTypes = {
  authorize: PropTypes.func.isRequired,
};

export default Login
