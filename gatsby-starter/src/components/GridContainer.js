import styled from 'styled-components';


export default styled.div`
  display: grid;
  width: 100%;
  max-width: 75rem;
  margin: 0 auto;
  padding: 0 1rem;

  @media (min-width: 600px) {
    padding: 0 2rem;
  }
`
