import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledAccordionTitle = styled.div`
  padding: 0.5rem;
  background: var(--color-primary);
  
  &:hover {
    cursor: pointer;
    background: var(--color-primary-dark);
  }
`;

class AccordionTitle extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { children, clickHandler, dbuId } = this.props
    return (
      <StyledAccordionTitle onClick={() => clickHandler(dbuId)}>{ children }</StyledAccordionTitle>
    )
  }
}

AccordionTitle.propTypes = {
  children: PropTypes.node,
  clickHandler: PropTypes.func,
  dbuId: PropTypes.string
}

export default AccordionTitle
