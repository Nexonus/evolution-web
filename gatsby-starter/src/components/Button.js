import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledButton = styled.button`
  border: 0;
  padding: 0.5rem;
  background-color: var(--color-primary);
  color: var(--color-light-grey);
  
  &:hover {
    cursor: pointer;
    background: var(--color-primary-dark);
  }
`;

class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { clickHandler, children, className } = this.props;
    return (
      <StyledButton onClick={() => clickHandler()} className={className}>{ children }</StyledButton>
    )
  }
}

Button.propTypes = {
  clickHandler: PropTypes.func.isRequired,
  children: PropTypes.node,
  className: PropTypes.string
};

export default Button
