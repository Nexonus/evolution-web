import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledWelcome = styled.div`
  padding: 1rem;
  background: var(--color-mediumlight-grey)
`;

class DashboardWelcome extends React.Component {
  render() {
    const { userName, text } = state.props
    return (
      <StyledWelcome>
        <h3>Welcome {userName}</h3>
        <p>{text}</p>
      </StyledWelcome>
    )
  }
}

DashboardWelcome.propTypes = {
  children: PropTypes.node,
  userName: PropTypes.string,
  text: PropTypes.string
}

export default DashboardWelcome
