import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Button from './Button'


const StyledForm = styled.form`
  display: grid;
  width: 100%;
  max-width: 75rem;
  margin: 0 auto 1.5rem;
  grid-gap: 1rem;
`

const StyledFormTop = styled.div``

const StyledFormBottom = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 1rem;
`

const StyledLabel = styled.label`
  display: block;
  width: 100%
`

const StyledSelect = styled.select`
  width: 100%;
  margin-top: 0.25rem;
  margin-bottom: 0.5rem;
  border: 0;
  padding: 0.2rem 0;
  background: white;
`

const StyledInput = styled.input`
  margin-top: 0.25rem;
  border: 0;
  padding: 0.2rem;
  width: 100%;
`

class ColumnCreator extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      columnName: null,
      columnType: null,
    }
  }

  handleChange(event) {
    this.setState({
      columnName: event.target.value
    })
  }

  handleSelectChange(event) {
    this.setState({
      columnType: event.target.value
    })
  }

  handleSubmit(event) {
    event.preventDefault()
    this.createColumn()
  }

  createColumn() {
      fetch('http://35.234.120.57:8080/api/'+ this.props.userId +'/db/' + this.props.dbName + '/table/'+ this.props.tableName +'/addColumn', {
        method: 'post',
        body: JSON.stringify({
            columnName: this.state.columnName,
            columnType: this.state.columnType
        })
      })
        .then(results => {
          return results.json()
        })
        .then(data => {
          // @todo re-render table component to show new column
        })
    }

  render() {
    const { columnType } = this.state
    const { closeColumnEditor } = this.props
    return (
      <StyledForm id="columnCreateFrom" name="columnCreateFrom" onSubmit={this.handleSubmit.bind(this)}>
        <StyledFormTop>
          <StyledLabel>
            Select Data Type:
            <StyledSelect value={columnType} onChange={this.handleSelectChange.bind(this)}>
              <option value="string">String</option>
              <option value="int">Integer</option>
              <option value="float">Float</option>
              <option value="bool">Boolean</option>
              <option value="date">Date</option>
            </StyledSelect>
          </StyledLabel>
          <label htmlFor="dbName">Column name:</label>
          <StyledInput type="text" id="columnName" name="columnName" onChange={this.handleChange.bind(this)} />
        </StyledFormTop>
        <StyledFormBottom>
          <Button clickHandler={() => {closeColumnEditor()}}>Cancel</Button>
          <Button type="submit" clickHandler={() => {}}>Create</Button>
        </StyledFormBottom>
      </StyledForm>
    )
  }
}

ColumnCreator.propTypes = {
  closeColumnEditor: PropTypes.func.isRequired,
  userId: PropTypes.number.isRequired,
  dbName: PropTypes.string,
  tableName: PropTypes.string
}

export default ColumnCreator
