import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Button from './Button'


const StyledButton = styled(Button)``

const StyledContainer = styled.div`
  ${StyledButton} {
    margin-top: 0.5rem;
    width: 100%;  
  }
`;

const StyledInput = styled.input`
  margin-top: 0.5rem;
  border: 0;
  padding: 0.2rem;
  width: 100%;
`;

class DatabankCreator extends React.Component {
  constructor(props) {
    super(props);
  }

  handleChange(event) {
    this.setState({
      value: event.target.value
    })
  }

  handleSubmit(event) {
    event.preventDefault()
    this.createNewDB()
  }

  //getting CORS problem
  createNewDB() {
    fetch('http://35.234.120.57:8080/api/' + this.props.userId + '/db/' + this.state.value + '/create', {
      method: "post"
    })
      .then(results => {
        return results.json()
      })
      .then(data => {
        this.props.fetchDbs()
      })
  }

  render() {
    return (
      <StyledContainer>
        <form id="databankCreateFrom" name="databankCreateFrom"  onSubmit={this.handleSubmit.bind(this)}>
          <div>
            <label htmlFor="dbName">Enter name:</label>
            <StyledInput type="text" id="dbName" name="dbName" onChange={this.handleChange.bind(this)} />
          </div>
          <StyledButton type="submit" clickHandler={() => {}}>Create</StyledButton>
        </form>
      </StyledContainer>
    )
  }
}

DatabankCreator.propTypes = {
  children: PropTypes.node,
  userId: PropTypes.number.isRequired,
  fetchDbs: PropTypes.func
}

export default DatabankCreator
