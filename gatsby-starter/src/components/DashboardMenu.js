import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import GridContainer from '../components/GridContainer'
import AccordionItem from "./AccordionItem";
import AccordionTitle from "./AccordionTitle"
import AccordionBody from "./AccordionBody"
import Button from './Button'
import DatabankCreator from './DatabankCreator'
import TableCreator from './TableCreator'


const StyledButton = styled(Button)``

const StyledAccordion = styled(GridContainer)`
  padding: 0;
  grid-gap: 0.5rem;
  grid-template-columns: 1fr;
  grid-area: menu;
  
  ${StyledButton} {
    width: 100%;
    margin-top: 0.5rem;
  }
`;

class DashboardMenu extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      databases : []
    }
  }

  fetchDatabases() {
    fetch('http://35.234.120.57:8080/api/user/getDbs?userId=' + this.props.userId)
      .then(results => {
        return results.json()
      })
      .then(data => {
        this.setState({
          databases:Object.entries(data)
        })
      })
  }

  deleteDatabase(dbuId) {
    fetch('http://35.234.120.57:8080/api/' + this.props.userId + '/db/'+ dbuId +'/delete', {
      method: 'delete'
    })
      .then(results => {
        return results.json()
      })
      .then(data => {
        this.fetchDatabases()
      })
  }

  componentDidMount() {
    this.fetchDatabases()
  }

  renderDatabankMenu() {
    const { tableClickHandler } = this.props
    return (
      this.state.databases.map( (db, index) => (
        <AccordionItem key={index} dbName={db[1]["dbName"]}>
          <AccordionTitle isExpandTrigger={true}>
            <h4>{db[1]["dbName"]}</h4>
          </AccordionTitle>
          <AccordionBody tableClickHandler={tableClickHandler} dbName={db[1]["dbName"]}>
            <StyledButton clickHandler={() => this.deleteDatabase(db[1]["dbName"])}>Delete Database</StyledButton>
          </AccordionBody>
        </AccordionItem>
      ))
    )
  }

  render() {
    const { databases } = this.state;
    const { userId } = this.props;
    return (
      <StyledAccordion>
        <AccordionItem>
          <AccordionTitle isExpandTrigger={true}>
            <h5>Create Database</h5>
          </AccordionTitle>
          <AccordionBody>
            <DatabankCreator userId={userId} fetchDbs={this.fetchDatabases.bind(this)}/>
          </AccordionBody>
        </AccordionItem>
        <AccordionItem>
          <AccordionTitle isExpandTrigger={true}>
            <h5>Create Table</h5>
          </AccordionTitle>
          <AccordionBody>
            <TableCreator databases={databases} userId={userId} fetchDbs={this.fetchDatabases.bind(this)}/>
          </AccordionBody>
        </AccordionItem>
        {this.renderDatabankMenu()}
      </StyledAccordion>
    )
  }
}

DashboardMenu.propTypes = {
  children: PropTypes.node,
  tableClickHandler: PropTypes.func.isRequired,
  userId: PropTypes.number.isRequired
}

export default DashboardMenu

