import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import TableListItem from './TableListItem'


const StyledAccordionBody = styled(({ shown, ...rest }) => <div {...rest} />)`
 background: var(--color-primary-light);
  padding: ${props => props.shown ? "0.5rem" : "0"};
`

class AccordionBody extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      data: null
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.isExpanded && !prevProps.isExpanded && this.props.dbName) {
      this.fetchTables(this.props.dbName)
    }
  }

  fetchTables(dbName) {
    fetch('http://localhost:8080/api/'+ this.props.userId +'/db/'+ dbName +'/getTables')
      .then(results => {
        return results.json()
      })
      .then(data => {
        this.setState({
          data: Object.entries(data)
        })
      })
  }

  renderTables(table) {
    return <TableListItem
              key={table[0]}
              tableClickHandler={() => this.props.tableClickHandler(table[1], this.props.dbName)}
              tableName={table[1]}
              dbName={this.props.dbName}>
              {table[1]}
          </TableListItem>
  }

  render() {
    const { isExpanded, children } = this.props

    return (
      <StyledAccordionBody shown={ !!isExpanded }>
        { isExpanded && this.state.data !== null ? this.state.data.map(this.renderTables, this) : null }
        { isExpanded ? children : null }
      </StyledAccordionBody>
    )
  }
}

AccordionBody.propTypes = {
  children: PropTypes.node,
  tableClickHandler: PropTypes.func.isRequired,
  dbName: PropTypes.string,
  userId: PropTypes.number.isRequired,
  isExpanded: PropTypes.bool,
  shown: PropTypes.bool
}

export default AccordionBody
