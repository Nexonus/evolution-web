import React from 'react'
import styled from 'styled-components'


const StyledPageHeader = styled.h3`
  grid-area: header;
`

class PageHeader extends React.Component {
  render() {
    const { headline } = this.props
    return (
      <StyledPageHeader>{headline}</StyledPageHeader>
    )
  }
}

export default PageHeader
