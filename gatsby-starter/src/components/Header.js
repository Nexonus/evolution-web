import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import GridContainer from './GridContainer'
import Login from './Login'
import Logout from './Logout'


const StyledHeader = styled.header`
  padding: 1rem 0;
  background-color: var(--color-dark-grey);

  @media (min-width: 600px) {
    padding: 2rem 0;
  }
`

const StyledGridContainer = styled(GridContainer)`
  grid-row-gap: 1rem;
  grid-template-areas:
    "logo"
    "login";

  @media (min-width: 600px) {
    grid-template-columns: 300px 300px;
    grid-template-areas: "logo login";
    justify-content: space-between;
  }
`

const StyledHeadline = styled.h1`
  color: var(--color-primary);
`

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { authorize, logout, userName, isLoggedIn } = this.props
    return (
      <StyledHeader>
        <StyledGridContainer>
          <StyledHeadline>DataSQuirL</StyledHeadline>
            {!isLoggedIn ? <Login authorize={authorize} /> : null}
            {isLoggedIn ? <Logout userName={userName} logout={logout} /> : null}
        </StyledGridContainer>
      </StyledHeader>
    )
  }
}

Header.propTypes = {
  authorize: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  userName: PropTypes.string
}

export default Header
