import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledTableItem = styled.div`
  &:hover {
    font-weight: bold;
    cursor: pointer;
  }
`

class TableListItem extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { children, tableClickHandler, tableName, dbName } = this.props

    return (
      <StyledTableItem onClick={ () => {tableClickHandler(tableName, dbName)} }>{ children }</StyledTableItem>
    )
  }
}

TableListItem.propTypes = {
  children: PropTypes.node,
  tableClickHandler: PropTypes.func.isRequired,
  tableName: PropTypes.string.isRequired,
  dbName: PropTypes.string.isRequired
}

export default TableListItem
