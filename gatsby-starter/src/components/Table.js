import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import GridContainer from './GridContainer'
import TableColumnContent from './TableColumnContent'


const StyledTableContainer = styled.div`
  max-width: 40rem;
  padding-bottom: 1rem;
  overflow-x: scroll;
`

const StyledTable = styled(({ columnCount, ...rest }) => <GridContainer {...rest} />)`
  padding: 0;
  align-items: start;
  grid-template-columns: repeat(${props => props.columnCount ? props.columnCount : "6"}, auto [col-start]);
  min-width: 40rem;
`

const StyledColumn = styled(GridContainer)`
  padding: 0;
`

const StyledColumnHeader = styled.div`
  background: var(--color-primary-light);
  padding: 0.2rem;
  font-weight: bold;
`

class Table extends React.Component {
  constructor(props) {
    super(props)
  }

  renderColumns(column, index) {
    const { userId, dbName, tableName } = this.props
    return (
      <StyledColumn key={index}>
        <StyledColumnHeader>{column.header}</StyledColumnHeader>
        {column.content.map((content, index ) => {
          return <TableColumnContent
                  key={index}
                  userId={userId}
                  dbName={dbName}
                  tableID={tableName}>
                  {content.content}
                </TableColumnContent>
        })}
      </StyledColumn>
    )
  }

  render() {
    const { tableData } = this.props
    return (
      <StyledTableContainer>
        {tableData.columns.length > 0 ?
          <StyledTable columnCount={tableData.columns.length}>
            {tableData.columns.map(this.renderColumns, this)}
          </StyledTable>
          : <div>No Cols</div>}
      </StyledTableContainer>
    )
  }
}

Table.propTypes = {
  children: PropTypes.node,
  userId: PropTypes.number.isRequired,
  dbName: PropTypes.string,
  tableName: PropTypes.string,
  tableData: PropTypes.object.isRequired
}

export default Table
