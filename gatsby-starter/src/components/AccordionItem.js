import React from 'react'
import PropTypes from 'prop-types'

class AccordionItem extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isExpanded: false,
      tables: []
    }
  }

  clickHandler() {
    this.setState(prevState => ({
      isExpanded: !prevState.isExpanded
    }))
  }

  render() {
    const { children, dbName } = this.props
    const childrenWithProps = React.Children.map(children, child => {
      if (child.props.isExpandTrigger === true) {
        return React.cloneElement(child, {
          clickHandler: this.clickHandler.bind(this),
          dbName: dbName
        })
      } else {
        return React.cloneElement(child, {
          isExpanded: this.state.isExpanded,
          dbName: dbName
        })
      }
    })

    return (
      <div>{childrenWithProps}</div>
    )
  }
}

AccordionItem.propTypes = {
  children: PropTypes.node,
  dbName: PropTypes.string.isRequired
}

export default AccordionItem
