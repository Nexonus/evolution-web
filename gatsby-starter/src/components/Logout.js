import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import GridContainer from './GridContainer'
import Button from './Button'

const StyledLogout = styled(GridContainer)`
  padding: 0;
  grid-row-gap: 0.5rem;
  grid-template-areas:
    "greeting"
    "logout";
`

const StyledGreeting = styled.h5`
  grid-area: greeting;
  text-align: right;
  color: var(--color-primary);
`

const StyledButton = styled(Button)`
  grid-area: logout;
`

class Logout extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { userName, logout } = this.props
    return (
      <StyledLogout>
        <StyledGreeting>Welcome {userName}</StyledGreeting>
        <StyledButton clickHandler={logout} to="/">Logout</StyledButton>
      </StyledLogout>
    )
  }
}

Logout.propTypes = {
  logout: PropTypes.func.isRequired,
  userName: PropTypes.string
}

export default Logout
