import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

import GridContainer from './GridContainer'


const StyledFooter = styled.footer`
  padding: 1rem 0;
  background-color: var(--color-dark-grey);

  @media (min-width: 600px) {
    padding: 2rem 0;
  }
`

const StyledGridContainer = styled(GridContainer)`
  grid-row-gap: 1rem;
  grid-column-gap: 1rem;
  grid-template-areas:
    "logo"
    "input";

  @media (min-width: 600px) {
    grid-template-columns: 200px 1fr;
    grid-template-areas: "logo input";
    align-items: center;
  }
`

const StyledText = styled.p`
  color: var(--color-primary);
`

class Footer extends React.Component {
  render() {
    return (
      <StyledFooter>
        <StyledGridContainer>
          <StyledText>ITECH Project</StyledText>
        </StyledGridContainer>
      </StyledFooter>
    );
  }
}

export default Footer
