import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledTableColumnContent = styled.div`
  padding: 0.2rem;
`

const StyledInput = styled.input`
  width: 100%;
  border: 0;
  padding: 0.1rem;
  
  &:hover {
    cursor: pointer;
  }
`

class TableColumnContent extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      inputValue: this.props.children
    }
  }

  handleChange(event) {
    this.setState({
      inputValue: event.target.value
    })
  }

  handleFocus(event) {
    this.setState({
      prevValue: event.target.value
    })
  }

  handleBlur(event) {
    if (this.state.prevValue !== event.target.value) {
      this.updateTableColumnContent()
    }
  }


  // @TODO add real call
  updateTableColumnContent() {
    console.log('updateTableColumnContent')
    fetch('http://35.234.120.57:8080/api/'+ this.props.userId +'/db/' + this.props.dbName + '/table/'+ this.props.tableName +'/', {
      method: "post"
    })
      .then(results => {
        return results.json()
      })
      .then(data => {
        // reload or change state to show success or whatever
      })
  }

  render() {
    return (
      <StyledTableColumnContent>
        <StyledInput
          type="text"
          id="content"
          name="content"
          value={this.state.inputValue}
          onChange={this.handleChange.bind(this)}
          onFocus={this.handleFocus.bind(this)}
          onBlur={this.handleBlur.bind(this)} />
      </StyledTableColumnContent>
    )
  }
}

TableColumnContent.propTypes = {
  children: PropTypes.node,
  userId: PropTypes.number.isRequired,
  dbName: PropTypes.string,
  tableName: PropTypes.string
}

export default TableColumnContent
