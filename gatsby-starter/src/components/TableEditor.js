import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import GridContainer from './GridContainer'
import Button from './Button'
import Table from './Table'
import ColumnCreator from './ColumnCreator'


const StyledTableEditor = styled(GridContainer)`
  grid-area: editor;
  align-content: start;
  grid-gap: 0.5rem;
  padding: 1rem;
  background: var(--color-mediumlight-grey);
`

const StyledEditorMenu = styled(GridContainer)`
  padding: 0;
  grid-gap: 0.2rem;
  grid-template-columns: 3fr 1fr 1fr 1fr;
`

class TableEditor extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      columnEditorIsOpen: false,
      cellEditorIsOpen: false
    }
  }

  openColumnEditor() {
    this.setState({
      columnEditorIsOpen: true
    })
  }

  closeColumnEditor() {
    this.setState({
      columnEditorIsOpen: false
    })
  }

  deleteTable(dbName, tableName) {
    fetch('http://35.234.120.57:8080/api/'+ this.props.userId + '/db/'+ dbName +'/table/'+ tableName + '/delete', {
      method: 'delete'
    })
      .then(results => {
        return results.json()
      })
      .then(data => {
        // @todo re-render
      })
  }

  render() {
    const { columnEditorIsOpen } = this.state
    const { userId, dbName, tableName, tableData, closeEditor } = this.props
    return (
      <StyledTableEditor>
        <StyledEditorMenu>
          <h3>Edit Table {tableData.name}</h3>
          { !columnEditorIsOpen ?
            <Button clickHandler={() => this.openColumnEditor()}>Add Column</Button>
            : <div/> }
          <Button clickHandler={() => this.deleteTable(dbName, tableName)}>Delete Table</Button>
          <Button clickHandler={closeEditor}>Close Editor</Button>
        </StyledEditorMenu>
        { columnEditorIsOpen ?
          <ColumnCreator
            userId={userId}
            dbName={dbName}
            tableName={tableName}
            closeColumnEditor={this.closeColumnEditor.bind(this)}
          /> : null }
        <Table tableData={tableData} userId={userId} dbName={dbName} tableName={tableName}/>
      </StyledTableEditor>
    )
  }
}

TableEditor.propTypes = {
  closeEditor: PropTypes.func.isRequired,
  userId: PropTypes.number.isRequired,
  dbName: PropTypes.string,
  tableName: PropTypes.string,
  tableData: PropTypes.object
}

export default TableEditor
