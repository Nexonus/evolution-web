import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Button from './Button'


const StyledButton = styled(Button)``

const StyledContainer = styled.div`
  ${StyledButton} {
    margin-top: 0.5rem;
    width: 100%;  
  }
`

const StyledLabel = styled.label`
  display: block;
  width: 100%
`

const StyledSelect = styled.select`
  width: 100%;
  margin-top: 0.25rem;
  margin-bottom: 0.5rem;
  border: 0;
  padding: 0.2rem 0;
  background: white;
`

const StyledInput = styled.input`
  margin-top: 0.25rem;
  border: 0;
  padding: 0.2rem;
  width: 100%;
`

class TableCreator extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      dbName : ''
    }
  }

  handleChange(event) {
    this.setState({
      tableName: event.target.value
    })
  }

  handleSelectChange(event) {
    this.setState({
      dbName: event.target.value
    })
  }

  handleSubmit(event) {
    event.preventDefault()
    this.createNewTable()
  }

  createNewTable() {
    fetch('http://35.234.120.57:8080/api/'+ this.props.userId +'/db/' + this.state.dbName + '/table/'+ this.state.tableName +'/create', {
      method: 'post'
    })
      .then(results => {
        return results.json()
      })
      .then(data => {
        this.props.fetchDbs()
      })
  }

  render() {
    const { dbName } = this.state
    const { databases } = this.props
    return (
      <StyledContainer>
        <form id="tableCreateFrom" name="tableCreateFrom"  onSubmit={this.handleSubmit.bind(this)}>
          <div>
            <StyledLabel>
              Select Database:
              <StyledSelect value={dbName} onChange={this.handleSelectChange.bind(this)}>
                {databases.map( (db, index) => (
                  <option key={index} value="{db[1]['dbName']}">{db[1]["dbName"]}</option>
                ))}
              </StyledSelect>
            </StyledLabel>
            <label htmlFor="tableName">Enter name:</label>
            <StyledInput type="text" id="tableName" name="tableName" onChange={this.handleChange.bind(this)} />
          </div>
          <StyledButton type="submit" clickHandler={() => {}}>Create</StyledButton>
        </form>
      </StyledContainer>
    )
  }
}

TableCreator.propTypes = {
  children: PropTypes.node,
  databases: PropTypes.array.isRequired,
  userId: PropTypes.number.isRequired,
  fetchDbs: PropTypes.func
}

export default TableCreator
