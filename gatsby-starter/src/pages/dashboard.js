import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import GridContainer from '../components/GridContainer'
import DashboardMenu from '../components/DashboardMenu'
import PageHeader from '../components/PageHeader'
import TableEditor from '../components/TableEditor'
import DashboardWelcome from '../components/DashboardWelcome'


const StyledGridContainer = styled(GridContainer)`
  align-items: start;
  margin-top: 2rem;
  grid-gap: 0.5rem;
  grid-template-areas: 
    "header header"
    "menu menu"
    "editor editor";
  grid-template-columns: 1fr 3fr;
  
  @media (min-width: 600px) {
    grid-template-areas: 
    "header header"
    "menu editor";
  }
`;

class Dashboard extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      userId: this.props.userId,
      userName: this.props.userName,
      tableEditorIsOpen: false,
      openTableName: null,
      openDbName: null,
    }
  }

  closeTableEditor() {
    this.setState({
      tableEditorIsOpen: true
    })
  }

  tableClickHandler(tableName, dbName) {
    this.fetchTableData(this.state.userId, dbName, tableName)

    if(!this.state.tableEditorIsOpen) {
      this.setState({
        tableEditorIsOpen: true
      })
    }

    this.setState({
      openTableName: tableName,
      openDbName: dbName
    })
  }

  fetchTableData(userID, dbName, tableName ) {
    fetch('http://35.234.120.57:8080/api/'+ userID +'/db/' + dbName + '/table/' + tableName + '/getTable')
      .then(results => {
        return results.json()
      })
      .then(data => {
        if ( data.status !== 500 ) {
          this.setState({
            tableData: data
          })
        }
      })
  }

  render() {
    return (
      <StyledGridContainer>
        <PageHeader headline="Dashboard"/>
        <DashboardMenu userId={this.state.userId} tableClickHandler={this.tableClickHandler.bind(this)} />
        { this.state.tableEditorIsOpen && this.state.tableData ?
          <TableEditor userId={this.state.userId} dbName={this.state.openDbName} tableName={this.state.openTableName} tableData={this.state.tableData} closeEditor={this.closeTableEditor.bind(this)} />
          : <DashboardWelcome userName={this.state.userName} text="Select a databank to edit." /> }
      </StyledGridContainer>
    )
  }
}

Dashboard.propTypes = {
  children: PropTypes.func,
  userId: PropTypes.number.isRequired,
  userName: PropTypes.string.isRequired
}

export default Dashboard
