import React from 'react'
import styled from 'styled-components'
import GridContainer from '../components/GridContainer'

const StyledTextComponent = styled(GridContainer)`
  grid-row-gap: 0.5rem;
  margin-top: 2rem;
  text-align: center;
`;

const StyledParagraph = styled.p`
  margin: 1rem 0 0 0;
`;


class IndexPage extends React.Component {

  render() {
    return (
      <StyledTextComponent>
        <h2>DataSQirl</h2>
        <h3>Personal Databank Serivce</h3>
        <StyledParagraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tincidunt et
          ligula id feugiat. Nam leo arcu, condimentum vel lacus a, tempus lobortis ante.
          Curabitur magna turpis, posuere blandit mattis nec, tempus ut risus. Pellentesque sagittis
          odio at enim suscipit sollicitudin. Nam ac magna convallis, commodo tellus eleifend,
          egestas justo. Duis mollis non ipsum ac ullamcorper. Etiam malesuada purus id molestie vulputate.
        </StyledParagraph>
      </StyledTextComponent>
    )
  }
}

export default IndexPage
