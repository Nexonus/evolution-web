import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import styled, { injectGlobal } from 'styled-components'

import favicon from '../assets/favicon.jpg'
import globalStyles from '../config/styles'
import Header from '../components/Header'
import Footer from '../components/Footer'

import Dashboard from '../pages/dashboard'
import IndexPage from '../pages/index'


injectGlobal`${globalStyles}`;

const StyledWrapper = styled.div`
  height: 100%;
  display: grid;
  grid-template-rows: auto 1fr auto;
`;

const StyledMain = styled.main`
  background-color: var(--color-light-grey);
`;

class Layout extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isAuthorized: false
    }
  }

  authorize(user) {
    if (user) {
     this.setState({
       isAuthorized: true,
       userName: user.name,
       userId: user.Id
     })
    }
  }

  logout() {
    this.setState({
      isAuthorized: false,
      userName: null,
      userId: null
    })
  }

  render() {
    const { isAuthorized, userName, userId } = this.state
    return (
      <StyledWrapper>
        <Helmet
          title={this.props.data.site.siteMetadata.title}
          meta={[
            {name: 'description', content: 'stuff'},
            {name: 'keywords', content: 'more stuff'},
          ]}
        >
          <link rel="icon" type="image/png" href={favicon} />
        </Helmet>
        <Header authorize={this.authorize.bind(this)} logout={this.logout.bind(this)} isLoggedIn={isAuthorized} userName={userName}/>
        <StyledMain>
          {isAuthorized ? <Dashboard userId={userId} userName={userName} /> : <IndexPage/>}
        </StyledMain>
        <Footer/>
      </StyledWrapper>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.node
}

export default Layout

export const query = graphql`
  query SiteDataQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
