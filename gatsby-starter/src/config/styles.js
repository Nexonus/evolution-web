// Global Styles + reset css
const variables = {
  '--color-primary': '#E37222',
  '--color-primary-light': '#E88B4A',
  '--color-primary-dark': '#BA5E1C',
  '--color-error': '#cc0000',
  '--color-error-light': '#FFCCCC',
  '--color-error-dark': '#8c0000',
  '--color-border': '#cccccc',
  '--color-dark-grey': '#1c1c1c',
  '--color-medium-grey': '#999999',
  '--color-mediumlight-grey': '#E6E6E6',
  '--color-light-grey': '#f3f3f3',
  '--border-width-primary': '0.125rem',
  '--border-radius-primary': '0.1875rem',
  '--font-primary': 'raleway',
  '--font-secondary': ''
};

const variablesString = Object.keys(variables)
  .map((variable) => `${variable}: ${variables[variable]};`)
  .join('');

const resetCSS = `
/* RESET CSS */
  html, body, div, span, applet, object, iframe,
  h1, h2, h3, h4, h5, h6, p, blockquote, pre,
  a, abbr, acronym, address, big, cite, code,
  del, dfn, em, img, ins, kbd, q, s, samp,
  small, strike, strong, sub, sup, tt, var,
  b, u, i, center,
  dl, dt, dd, ol, ul, li,
  fieldset, form, label, legend,
  table, caption, tbody, tfoot, thead, tr, th, td,
  article, aside, canvas, details, embed,
  figure, figcaption, footer, header, hgroup,
  menu, nav, output, ruby, section, summary,
  time, mark, audio, video {
    margin: 0;
    padding: 0;
    border: 0;
    vertical-align: baseline;
    box-sizing: border-box;
  }
  /* HTML5 display-role reset for older browsers */
  article, aside, details, figcaption, figure,
  footer, header, hgroup, menu, nav, section {
    display: block;
  }
  body {
    line-height: 1;
  }
  ol, ul {
    list-style: none;
  }
  blockquote, q {
    quotes: none;
  }
  blockquote:before, blockquote:after,
  q:before, q:after {
    content: '';
    content: none;
  }
  table {
    border-collapse: collapse;
    border-spacing: 0;
  }
  `;


export default `
  :root {
    ${variablesString}
  }
  
  ${resetCSS}
  
  html,
  body,
  #___gatsby {
    height: 100%;
  }
`;
