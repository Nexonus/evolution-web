# Dummy Jsons

I added this as a source of dummy jsons, so I can mock fetches.

To use, install the http-server 
[https://www.npmjs.com/package/http-server]
` npm install http-server -g`

Call `http-server $PATH/dummy-jsons/ --cors` to start
 the mini server and allow cross-site scripting.
