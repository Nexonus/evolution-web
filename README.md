# Evolution-Web

Place any relevant docs for other developers here:

## Backend

The backend is located in /server

The detailed documentation can be found [here](./server/README.md)

## Frontend

The frontend is found in /gatsby-starter.
The frontend includes an additional README.md. 

To run the frontend locally ensure you have the
`node.js` javascript runtime environment
and `node package manager (npm)` installed
globally on your device.

```
# install Gatsby CLI globally
npm install -g gatsby-cli
``` 
 Then:

 + `cd /gatsby-starter`
 + `npm install` (installs all frontend dependencies)
 + `ǹpm run develop`
 + Navigate in browser to localhost:8000
 